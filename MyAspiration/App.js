import React, {useState} from 'react';
import { Button, StyleSheet, Text, TextInput, View, ScrollView } from 'react-native';
import { FlatList } from 'react-native-web';
import {AspirationItem} from './components/AspirationItem';
import AspirationInput from './components/AspirationInput';

export default function App() {
  const [enteredAspiration, setEnteredAspiration] = useState('');
  const [courseAspirations, setCourseAspirations] = useState([]);

  const AspirationInputHandler = aspirationTitle => {
    setEnteredAspiration(enteredText);
  };
  const addAspirationHandler =() =>{
    // console.log(enteredAspiration);
    // setCourseAspirations([...courseAspirations, enteredAspiration]);
    setCourseAspirations(currentAspiration => [
      ...courseAspirations,
      { key: Math.random().toString(), value: aspirationTitle}
    ])
  };
  return(
        <View style={styles.screen}>
            <View style={styles.inputContainer}>
                {/* <TextInput
                placeholder="My Aspiration from this module" 
                style={styles.input}
                onChangeText={AspirationInputHandler}
                value={enteredAspiration}
                /> */}
                <Button
                title='ADD'
                onPress={() => props.onAddAspiration(enteredAspiration)}
                />
                <AspirationInput onAddAspiration = {addAspirationHandler}/>
                <FlatList
                data={courseAspirations}
                renderItem = {itemData => 
                    <AspirationItem title = {itemData.item.value}/>
                }
                />
            </View>
            <ScrollView>
              {courseAspirations.map((aspiration)=> 
              <View key={aspiration} style={styles.listItem}>
                <Text key={aspiration}>{aspiration}</Text>
              </View>
              )}
            </ScrollView>

        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        padding: 50
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10
    },
    listItem: {
      padding: 10,
      marginVertical: 10,
      backgroundColor: '#ccc',
      borderColor: 'black',
      borderWidth: 1
    }
});