import React from 'react'
import { StyleSheet, View, TextInput } from 'react-native';
import { Button } from 'react-native-web';

const AspirationInput = (props) => {
    return(
        <View style={StyleSheet.inputContainer}>
            <TextInput
            placeholder="My Aspiration from this module"
            style={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiration}
            />
            <Button
            title='ADD'
            onPress={addAspirationHandler}
            />
        </View>
    )
}
export default AspirationInput
const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10
    }
});