import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './components/nameExport';
import divide from './components/defaultExport';
import Courses from './components/funcComponent';
export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text>Open up App.js to start working on your app!</Text>
      <Text>Result of Addition: {add(5, 6)}</Text>
      <Text>Result of Multiplication: {multiply(5, 8)}</Text>
      <Text>Result of division: {divide(10,2)}</Text>
      <StatusBar style="auto" /> */}
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
