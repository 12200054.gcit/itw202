import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { Button, FlatList, StyleSheet, Text,TextInput, View } from 'react-native';
import { AspirationItem } from './component/AspirationItem';
import Aspirationinput from './component/AspirationInput';

export default function App() {
  const [courseAspirations, setCourseAspirations] = useState([]); 
  const [isAddMode, setIsAddMode] = useState(false);
  

  const addAspirationHandler = aspirationTitle =>{
    setCourseAspirations(currentAspirations => [
      ...currentAspirations,
      {key: Math.random().toString(), value: aspirationTitle }
    ])
    setIsAddMode(false)
  };

  const removeAspirationHandler = aspirationKey => {
    setCourseAspirations(currentAspirations => {
      return currentAspirations.filter((aspiration)=> aspiration.key !== aspirationKey)
    })
  }
    return (
    <View style={styles.screen}>
      <Button title=' Add New Aspiration' onPress={() => setIsAddMode(true)} />
      <Aspirationinput visible= {isAddMode} onAddAspiration = {addAspirationHandler} />
      <FlatList
        data={courseAspirations}
        renderItem = {itemdata =>
        <AspirationItem id={itemdata.item.key} onDelete={removeAspirationHandler} title ={itemdata.item.value} />
      } />
      <StatusBar style="auto" />
        
    </View>
  );
}

const styles = StyleSheet.create({
  screen:{
    padding: 50,
    },
    inputContainer:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems:'center'
    },
    input:{
      width:'80%',
      borderColor: 'black',
      borderWidth: 1,
      padding: 10
    }
});
