import React, {useState} from 'react'
import { StyleSheet, TextInput, View, Alert, Dimensions, useWindowDimensions } from 'react-native'
import Colors from '../constants/colors';
import Primarybutton from '../components/ui/Primarybutton'
import Title from '../components/ui/Title';
import Card from '../components/ui/Card';
import InstructionText from '../components/ui/InstructionText';
import { KeyboardAvoidingView, ScrollView } from 'react-native';

function StartGameScreen({onPickNumber}) {
    const [enteredNumber, setEnteredNumber] = useState('');

    const {width, height} = useWindowDimensions();

    function numberInputHandler(enteredText){
      setEnteredNumber(enteredText);
    }
    function resetInputHandler(){
      setEnteredNumber('')
    }
    function confirmInputHandler(){
      const choosenNumber = parseInt(enteredNumber);

      if (isNaN(choosenNumber) || choosenNumber <= 0 || choosenNumber > 99)
      {
        Alert.alert('Invalid number!', 'Number has to be between 1 and 99.',
        [{text: 'okay', style: 'destructive', onPress: resetInputHandler}]
        )
        return;
      }
      // console.log('valid number!')
      onPickNumber(choosenNumber)
    }
    const marginTopDistance = height < 380 ? 30: 100;

  return (
    <ScrollView style={styles.screen}>
    <KeyboardAvoidingView style={styles.screen} behavior="position">
    <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
      <Title>Guess My Number</Title>
      <Card style={styles.inputContainer}>
        <InstructionText>Enter a Number</InstructionText>
        <TextInput
          style={styles.numberInput}
          maxLength={2}
          keyboardType="number-pad"
          autoCapitalize='none'
          autoCorrect={false}
          onChangeText={numberInputHandler}
          value={enteredNumber}
        />
          <View style={styles.buttonContainer}>
            <View style={styles.buttonsContainer}>
              <Primarybutton onPress={resetInputHandler}>Reset</Primarybutton>
            </View>
            <View style={styles.buttonsContainer}>
              <Primarybutton onPress={confirmInputHandler}>Confirm</Primarybutton>
            </View>
          </View>
      </Card>
    </View>
    </KeyboardAvoidingView>
    </ScrollView>
  )
}

export default StartGameScreen;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  screen: {
    flex: 1
  },
  rootContainer: {
    flex: 1,
    marginTop: deviceHeight < 380 ? 30: 100,
    alignItems: 'center',
  },
  instructionText: {
    color: Colors.accent500,
    fontSize: 24
  },

  inputContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 36,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary800,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25,
  },

  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor: Colors.accent500,
    borderBottomWidth: 2,
    color: Colors.accent500,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'

  },
  buttonContainer: {
    flexDirection: 'row',
  },
  buttonsContainer: {
    flex: 1,

  }

})