import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const MyComponent = () => {
    const greeting = 'Getting started\nwith react\nnative!';
    const name = 'My name is Kesang \nChoden';
    return (
        <View>
            <Text style = {styles.textStyle}>{greeting}</Text>
            <Text style = {{fontSize:20, textAlign: 'center', paddingRight:80, fontWeight:'bold'}}>{name}</Text>
        </View>
    )
};
const styles = StyleSheet.create({
    textStyle : {
      fontSize: 45,
      textAlign: 'center',
      paddingRight: 60,
    }
  });
export default MyComponent;